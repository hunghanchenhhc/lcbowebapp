package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        if(type.equals(LiquourType.WINE)){
            brands.add("Adrianna Vineyard");
            brands.add(("J. P. Chenet"));

        }else if(type.equals(LiquourType.WHISKY)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Walker");

        }else if(type.equals(LiquourType.BEER)){
            brands.add("Corona");
            brands.add("Budweiser");
            brands.add("Heineken");
            brands.add("Coors");
            brands.add("Canadian");

        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
